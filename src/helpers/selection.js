exports.rangeWrap = (el, alter, direction, granularity) => {
  const sel = document.getSelection()
  if (alter) {
    sel.modify(alter, direction, granularity)
  }
  const range = sel.getRangeAt(0)
  range.deleteContents()
  range.surroundContents(el)
  range.collapse()
}

exports.rangeInsert = (el, alter, direction, granularity) => {
  const sel = document.getSelection()
  if (alter) {
    sel.modify(alter, direction, granularity)
  }
  const range = sel.getRangeAt(0)
  range.deleteContents()
  range.insertNode(el)
  range.collapse()
}

exports.caretPlaceAfterNode = (node) => {
  if (typeof document.getSelection !== 'undefined') {
    const range = document.createRange()
    range.setStartAfter(node)
    range.collapse(true)
    const selection = document.getSelection()
    selection.removeAllRanges()
    selection.addRange(range)
  }
}

exports.caretInsertNode = (node) => {
  if (typeof document.getSelection !== 'undefined') {
    const sel = document.getSelection()
    if (sel.rangeCount) {
      let range = sel.getRangeAt(0)
      range.collapse(false)
      range.insertNode(node)
      range = range.cloneRange()
      range.selectNodeContents(node)
      range.collapse(false)
      sel.removeAllRanges()
      sel.addRange(range)
    }
  }
}

exports.getWord = () => {
  const sel = document.getSelection()
  if (!sel || sel.rangeCount < 1) return true
  const range = sel.getRangeAt(0)
  const node = sel.anchorNode
  const wordRegexp = /\w*$/

  while ((range.startOffset > 0) && range.toString().match(wordRegexp)) {
    range.setStart(node, (range.startOffset - 1))
  }

  if (!range.toString().match(wordRegexp)) {
    range.setStart(node, range.startOffset + 1)
  }

  while ((range.endOffset < node.length) && range.toString().match(wordRegexp)) {
    range.setEnd(node, range.endOffset + 1)
  }

  if (!range.toString().match(wordRegexp)) {
    range.setEnd(node, range.endOffset - 1)
  }

  return range.toString()
}

exports.watchForHash = (el) => {
  let
    precedingChar,
    sel,
    range,
    hasSpace

  if (document.getSelection) {
    sel = document.getSelection()
    if (sel.rangeCount > 0) {
      range = sel.getRangeAt(0).cloneRange()
      range.collapse(true)
      range.setStart(el, 0)
      precedingChar = range.toString().slice(-1)
      hasSpace = /\s/.test(range.toString().slice(-2))
    }
  }
  if (hasSpace && precedingChar === '#') {
    return true
  }
}
