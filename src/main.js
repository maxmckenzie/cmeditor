import Vue from 'vue'
import App from './App.vue'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

if (process.NODE_ENV === 'production') {
  Sentry.init({
    dsn: 'https://ab1b6f62b61f40fcb93f9b41797bff71@sentry.io/1887528',
    integrations: [new Integrations.Vue({ Vue, attachProps: true })]
  })
}

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
