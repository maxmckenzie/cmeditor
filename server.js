var history = require('connect-history-api-fallback')
var express = require('express')

process.title = 'cmEditor'

console.log(process.title)

var app = express()
app.use(history({
  index: 'index.html',
  logger: console.log.bind(console)
}))

app.use('/', express.static(process.cwd() + '/dist'))

app.get('/', function (req, res) {
  res.sendFile(process.cwd() + '/dist/index.html')
})

app.listen(process.env.PORT || 8080)
