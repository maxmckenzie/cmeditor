# cmeditor

[![pipeline status](https://gitlab.com/maxmckenzie/cmeditor/badges/master/pipeline.svg)](https://gitlab.com/maxmckenzie/cmeditor/commits/master)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


https://alex-d.github.io/Trumbowyg/documentation/#api

https://stackoverflow.com/questions/2920150/insert-text-at-cursor-in-a-content-editable-div